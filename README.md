# gamedata

Saulc says `actually unknow what acsvln and lawncable want`

The idea is that we have one repo for all gamadata.
When we realease a new version of the game or test the data from this repo would then be `compiled` to [server-](https://gitlab.com/TMW2/serverdata) and [client-data](https://gitlab.com/TMW2/clientdata).

That is big step and process but we can get following out of it:
- We can create our own imporved format for this repo*
    - We can make that format easy to understand so we gat more contributers and they can do only one merge request for an item instead of two
- We only have to maintain one repo and client & serverdata will never get get out of sync again
- If we happen to use JSON in this format we can use JSON SCHEMES for atocompletion and linting/validation


* that is the long process - combine all the data and develop a better and easier format
As example using folders for each object that cointains all the files for the object.
like on clothes:
Pseudo format
```
/tophat/
- properies.json //Item data and stats
- sprites.json // Information about the sprite like where to find what position or animation
- spritemap.png
- item.png
```

Lets discuss it in the issues here and on discord;
See you there