# folderNjson - approach IDEA
**This is just an rough idea - we gotta adjust it to our needs if we decide to use it.**

Everything is in directorys - every entity (monster, item, weapon, cloth, quest) has its own folder.

### Why JSON

- smaller filesize than xml
- We can use [JSON Schema](https://json-schema.org/) for autocomplete, linting and validation


### Types of Files

###### `properties.json`
The main file that describes what type it is what id and name it has, the description and so on.
Added sections in `properties.json` depending on the item:

Clothes
- stats (how much defense it has)

Weapons
 - damage value

Item
 - types
    - potion { effectId:"REGEN_HEALTH", duration:400, effectMultiplier:1 }
    - food { minHealth:40, maxHealth:60, consumeDuration:2000} (consumeDuration - the time how long the health takes to arrive)
 

Misc
- Item cards


###### `sprites.json` (wearables, monsters, effects)
File that descibes the animations and normal spritemapping

###### `icon.png` (items)

{todo here is still info missing}



## Known problems of this model idea / what we still need to account for:

- Items that use the same sprite / other items that have variations
    - we could solve that by either detecting double images while compilation
    and coping only one and referencing it or by having all images in a different folder tree

